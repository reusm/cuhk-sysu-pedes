FROM python:3.10.9-slim AS base
RUN apt update && apt install git -y && rm -rf /var/lib/apt/lists/*

#######################################################
######################### DEV #########################
#######################################################
FROM base AS dev
COPY ./requirements.dev.txt requirements.txt
COPY ./requirements.test.txt requirements.test.txt 
RUN pip install --no-cache-dir -r ./requirements.txt \
 && rm requirements*.txt
#######################################################

#######################################################
######################## BUILT ########################
#######################################################
FROM base AS built
COPY . /cuhk_sysu_pedes
WORKDIR /cuhk_sysu_pedes
RUN pip install .
#######################################################

#######################################################
######################### TEST ########################
#######################################################
FROM built AS test
COPY ./requirements.test.txt requirements.txt 
RUN pip install --no-cache-dir -r ./requirements.txt \
 && rm requirements.txt
#######################################################
