# Docker images

This repo has a [Dockerfile](./Dockerfile) so you can easily adapt or extend the base images used in this repo. Plus, there is a [docker compose](./docker-compose.yml) file to quickly starts container according to your usage.

## Dockerfile

The Dockerfile has multiple stages:

- `base`: simply Python 3.10 and git.
- `dev` is built from `base`, it has all the dependencies for development (linter, formatter, ipykernel ...). You can use this image as a base for your own customized development environment.
- `built` is built from `base`. It is used for the annotations / dataset generations.
- `test` is built from `built`. It is used for the testing.

## Compose file

For ease of usage, we added a  file. There are four services here:

- `dev`: launch the "heaviest" container with dev dependencies.
- `test`: just launch the container, it runs the tests.
- `create` runs the annotations which are created in the `outputs` Docker volume. It outputs the `.csv` annotation and the MMLab dataset in a `outputs` folder.
- `retrieve` is a super lightweight service used to retrieve the `outputs` folder. Just use `docker cp retrieve:/outputs <path/on/host>` to get your annotations on the host machine.

> **NOTE** You must fill the [environment file](./.env) to use the containers.