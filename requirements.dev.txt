ipykernel==6.20.2
scipy==1.7.3
PyYAML==6.0
pylint==2.14.5
black
pandas==1.5.0
seaborn==0.12.0
-r requirements.test.txt
