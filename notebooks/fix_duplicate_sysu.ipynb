{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Imports\n",
    "from collections import Counter\n",
    "from pathlib import Path\n",
    "\n",
    "import numpy as np\n",
    "from scipy.io import loadmat\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the G100 test samples\n",
    "test_path = Path(\n",
    "    \"./data/CUHK-SYSU/annotation/test/train_test/TestG100.mat\"\n",
    ")\n",
    "\n",
    "test_annotations = loadmat(test_path)[\"TestG100\"][0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Fix problems in the CUHK-SYSU.\n",
    "\n",
    "The aim of this notebook is to provide a a list of tuples containing 3 elements :\n",
    "\n",
    "- `query_person_id: int` is the person ID of the test sample with a problem.\n",
    "- `frame_id: int` is the frame ID in the gallery that has a problem.\n",
    "- `replacement_frame_annotation: np.ndarray` is the replacement frame annotation of the problematic frame annotation with `frame_id` frame ID in the gallery with `query_person_id` person ID.\n",
    "\n",
    "There are two types of problems in the SYSU annotations:\n",
    "\n",
    "- Some galleries contains the query frame in them.\n",
    "- Some galleries has duplicated frames.\n",
    "\n",
    "So firstly we [assert the diagnosed problem](#diagnose-problems), then we [select the distractor replacements](#find-matching-replacement). Finally, we [find the problematic frames](#locate-the-problematic-frames-in-galleries) to make the tuple and [export it](#output-fix).\n",
    "\n",
    "**NOTE**: The test set is the one with the gallery size of 100 frames."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Diagnose problems\n",
    "\n",
    "In this section we will find the `query_person_id` person IDs of the sample with one of the two problems."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Galleries containing the query frame in them\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Helper function\n",
    "def query_frame_is_in_its_gallery(sample: np.ndarray) -> bool:\n",
    "    gallery_frame_ids = [\n",
    "        frame_annotation[\"imname\"][0]\n",
    "        for frame_annotation in sample[\"Gallery\"][0]\n",
    "    ]\n",
    "    query_frame_id = sample[\"Query\"][\"imname\"][0][0][0]\n",
    "\n",
    "    return query_frame_id in gallery_frame_ids"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['p10354', 'p484']"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "person_ids_with_query_frame_in_gallery = [\n",
    "    sample[\"Query\"][\"idname\"][0][0][0]\n",
    "    for sample in test_annotations\n",
    "    if query_frame_is_in_its_gallery(sample)\n",
    "]\n",
    "person_ids_with_query_frame_in_gallery"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Galleries with duplicate"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Assert gallery annotations are the right size\n",
    "for gallery in test_annotations[\"Gallery\"]:\n",
    "    assert len(gallery.squeeze()) == 100\n",
    "GALLERY_SIZE = 100\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Helper function\n",
    "def get_frame_ids_from_gallery(\n",
    "    gallery: np.ndarray,\n",
    ") -> list[str]:\n",
    "    return [\n",
    "        frame_annotation[\"imname\"][0]\n",
    "        for frame_annotation in gallery\n",
    "    ]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['p3091', 'p1226', 'p4667', 'p1489']"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "person_ids_with_duplicate_in_gallery = [\n",
    "    query[\"idname\"][0][0][0]\n",
    "    for query, gallery in zip(\n",
    "        test_annotations[\"Query\"], test_annotations[\"Gallery\"]\n",
    "    )\n",
    "    if GALLERY_SIZE\n",
    "    != len(set(get_frame_ids_from_gallery(gallery.squeeze())))\n",
    "]\n",
    "person_ids_with_duplicate_in_gallery"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Find replacement in an already distractor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Helper functions\n",
    "def convert_person_id_int_to_str(person_id_int: int) -> str:\n",
    "    return f\"p{person_id_int}\"\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Frame annotation should be:\n",
    "\n",
    "1. A distractor\n",
    "2. Not be in the gallery (for simplicity, not be in any problematic gallery)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set forbidden frames"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Find forbidden (gallery with problem)\n",
    "galleries_and_person_ids = [\n",
    "    (\n",
    "        sample[\"Gallery\"][0],\n",
    "        str(sample[\"Query\"][\"idname\"][0][0][0]),\n",
    "    )\n",
    "    for sample in test_annotations\n",
    "]\n",
    "\n",
    "galleries_with_problems = [\n",
    "    gallery\n",
    "    for gallery, person_id in galleries_and_person_ids\n",
    "    if person_id\n",
    "    in (\n",
    "        person_ids_with_duplicate_in_gallery\n",
    "        + person_ids_with_query_frame_in_gallery\n",
    "    )\n",
    "]\n",
    "assert len(galleries_with_problems) == len(\n",
    "    person_ids_with_duplicate_in_gallery\n",
    ") + len(person_ids_with_query_frame_in_gallery)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "forbidden_frame_ids = [\n",
    "    str(frame_annotation[\"imname\"][0])\n",
    "    for gallery in galleries_with_problems\n",
    "    for frame_annotation in gallery\n",
    "]\n",
    "assert len(forbidden_frame_ids) == 100 * len(\n",
    "    galleries_with_problems\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Find valid distractors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "260484"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "candidate_distractors = [\n",
    "    frame_annotation\n",
    "    for gallery, _ in galleries_and_person_ids\n",
    "    for frame_annotation in gallery\n",
    "    if (\n",
    "        frame_annotation[\"imname\"][0]\n",
    "        not in forbidden_frame_ids\n",
    "        and not frame_annotation[\"idlocate\"].any()\n",
    "    )\n",
    "]\n",
    "len(candidate_distractors)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Select the replacements from candidate distractors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([(array(['s9732.jpg'], dtype='<U9'), array([], shape=(1, 0), dtype=float64), array([], shape=(1, 0), dtype=float64)),\n",
       "       (array(['s2382.jpg'], dtype='<U9'), array([], shape=(1, 0), dtype=float64), array([], shape=(1, 0), dtype=float64)),\n",
       "       (array(['s2071.jpg'], dtype='<U9'), array([], shape=(1, 0), dtype=float64), array([], shape=(1, 0), dtype=float64)),\n",
       "       (array(['s16210.jpg'], dtype='<U10'), array([], shape=(1, 0), dtype=float64), array([], shape=(1, 0), dtype=float64)),\n",
       "       (array(['s12642.jpg'], dtype='<U10'), array([], shape=(1, 0), dtype=float64), array([], shape=(1, 0), dtype=float64)),\n",
       "       (array(['s6828.jpg'], dtype='<U9'), array([], shape=(1, 0), dtype=float64), array([], shape=(1, 0), dtype=float64))],\n",
       "      dtype=[('imname', 'O'), ('idlocate', 'O'), ('ishard', 'O')])"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "distractors_replacements = np.array(\n",
    "    [\n",
    "        candidate_distractors[i]\n",
    "        for i in range(len(galleries_with_problems))\n",
    "    ]\n",
    ")\n",
    "distractors_replacements"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Locate the problematic frames in galleries."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Locate gallery duplicate\n",
    "\n",
    "For locating the query frame in gallery, this is straight forward. The problematic frame annotation has the same frame ID of the query frame.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "galleries_with_duplicate = [\n",
    "    gallery[0]\n",
    "    for query, gallery in zip(\n",
    "        test_annotations[\"Query\"], test_annotations[\"Gallery\"]\n",
    "    )\n",
    "    if query[0][\"idname\"][0][0]\n",
    "    in person_ids_with_duplicate_in_gallery\n",
    "]\n",
    "assert len(galleries_with_duplicate) == len(\n",
    "    person_ids_with_duplicate_in_gallery\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "def find_first_duplicate(frame_ids: list[str]) -> str:\n",
    "    counter_frame_ids = Counter(frame_ids)\n",
    "    duplicate_frame_id = [\n",
    "        frame_id for frame_id, count in counter_frame_ids.items() if count == 2\n",
    "    ]\n",
    "    assert len(duplicate_frame_id) == 1\n",
    "    return duplicate_frame_id[0]\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['s3981.jpg', 's11888.jpg', 's5957.jpg', 's12295.jpg']"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "frame_ids_duplicated_in_gallery = [\n",
    "    find_first_duplicate(get_frame_ids_from_gallery(gallery))\n",
    "    for gallery in galleries_with_duplicate\n",
    "]\n",
    "frame_ids_duplicated_in_gallery\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Locate the query frame in gallery\n",
    "\n",
    "For locating the duplicated frame, we inspect the galleries with duplicate and get the (only) duplicated frame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['s14430.jpg', 's888.jpg']"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "frame_ids_with_query_in_gallery = [\n",
    "    query[0][\"imname\"][0][0]\n",
    "    for query in test_annotations[\"Query\"]\n",
    "    if query[0][\"idname\"][0]\n",
    "    in person_ids_with_query_frame_in_gallery\n",
    "]\n",
    "frame_ids_with_query_in_gallery"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Output fix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [],
   "source": [
    "assert len(frame_ids_with_query_in_gallery) + len(\n",
    "    frame_ids_duplicated_in_gallery\n",
    ") == len(galleries_with_problems)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(6, 3)"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "replacements_record_info = np.array(list(\n",
    "    (query_person_id, gallery_frame_id, distractors_replacements)\n",
    "    for query_person_id, gallery_frame_id, distractors_replacements in zip(\n",
    "        person_ids_with_duplicate_in_gallery + person_ids_with_query_frame_in_gallery,\n",
    "        frame_ids_duplicated_in_gallery + frame_ids_with_query_in_gallery,\n",
    "        distractors_replacements,\n",
    "    )\n",
    "))\n",
    "replacements_record_info.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "output_file = Path(\"./src/cuhk_sysu_pedes/data/replacements_record_info.npy\")\n",
    "np.save(output_file, replacements_record_info)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Line to import\n",
    "imported_record_info = np.load(output_file, allow_pickle=True)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.9"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "949777d72b0d2535278d3dc13498b2535136f6dfe0678499012e853ee9abcab1"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
