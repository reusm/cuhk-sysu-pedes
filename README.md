# CUHK-SYSU-PEDES

This package fuses the overlapped annotations from CUHK-SYSU (Detection + ReID task) and CUHK-PEDES (Text-based query ReID).

These new annotations are fused to train model on a super task of ReID: a ReID that can detect people on frame and search them from an image query or/and a text query.

> **WARNING**: This repo does not give any data or annotations from [CUHK-SYSU](https://github.com/ShuangLI59/person_search) and [CUHK-PEDES](https://github.com/ShuangLI59/Person-Search-with-Natural-Language-Description). You need the permissions from their respective authors.


## How to use this package

The point of this package is to produce the fused annotations in a `.csv` (dataframes friendly format) or `.json` (Open MMLab format). Additionnaly, you can generate the whole dataset folder which is [OpenMMLab](https://mmengine.readthedocs.io/en/latest/advanced_tutorials/basedataset.html#the-standard-data-annotation-file) compliant.

### Docker is setting up everything

> **PREREQUISITE**: you need to fill the [`.env`](.env) file with the path of your local dataset folder and the desired output format - see [this section](#dataframe-and-coco-export).

We added Docker files (a Dockerfile and a compose) to easily create annotations or the whole dataset, or setup the dev environment. The [bash script](/scripts/build_annotations_from_docker.sh) uses these images / containers to produce the desired annotations. You just need to type `scripts/build_annotations_from_docker.sh outputs` from root directory of this repo, `outputs` is the folder containing the dataset and `.csv` annotations.

For more informaiton, read the [docker `README.md`](README.docker.md).

### Install the package and run the script

Alternatively, you can install this package in a python environment of your choice. Then you can use the built-in package CLI tool `python -m cuhk_sysu_pedes.cli`. For more information, run  `python -m cuhk_sysu_pedes.cli -h`. You must have Python 3.10+.

## Files organization

- `src`, code of the packages
- `notebooks`, illustrations of the fusing method and some data visualization.
- `data`, where I put my datasets folders. It is flexible, for the building annotations script, you do not have to respect this location. For running notebooks without changing anything, you should respect this `data` folder with `CUHK-SYSU` and `CUHK-PEDES` folders inside. Also, to run test (without docker container), you need to respect this.
- `tests` some weak unit tests to be sure that the annotations are fused correctly.

## Dataframe and COCO export

There are two possible output formats: `.csv` and `.json`:

- **Why `.json`?** Historically, some papers used the [mmdetection](https://github.com/open-mmlab/mmdetection) library to run their Detection + ReID models ([AlignPS implementation](https://github.com/daodaofr/AlignPS)). So, the annotations are used with COCO detection format. They add a `person_id` key for each entry in `annotations` to add the ReID annotations. For more information about COCO format, check [COCO documentation](https://cocodataset.org/#format-data). Since we want to be compliant with annotations from [OpenMMLab](https://mmengine.readthedocs.io/en/latest/advanced_tutorials/basedataset.html#the-standard-data-annotation-file) version 2, we also provide a `mmlab` format.
- **Why `.csv`?** This is a straightforward format for `pandas`' `DataFrame`. We directly implement our *native* annotations, see [the dedicated section](#native-format-annotations) for more information.

## Native format annotations

This format contains information about the annotations only. There is no meta data such as the path of the files or the size of the images. **Although, it can be easily extended based on `frame_id`** since the `frame_id` is the number taken from the filename of the whole frame: `s<frame_id>.jpg`.

Each detection is uniquely identified with two keys: `frame_id` and `person_id`. Namely, given a frame and a person there is only one detection. If you prefer to have a single key/index for each detection, you can use the `reset_index()` method on the annotation `pd.DataFrame`.

> **NOTE** This is not true for training detections. SYSU includes a lot of unlabaled detection - they do not have a person ID - for training the detector correctly. Those detection has a person ID sets to `-1` and there are multiple detections of peole on a train frame without person ID.

Then each detection has those values:

- `bbox_x`, `bbox_y`, `bbox_h` and `bbox_w` (`uint16`) for the location of the person on the frame.
- `caption_1` and `caption_2` for the pair of captions for each person.
- `is_hard` indicates if the detection is challenging. This annotation comes from `CUHK-SYSU` dataset.
- `split_sysu` indicates the split from CUHK-SYSU. There is one category for training (`train`) and two categories for testing (`query` and `gallery`).
- `split_pedes` indicates the split from CUHK-PEDES. There are three categories: `train`, `test` and `val`

For visualize those annotations in `pd.DataFrame` you can play with the [fusing notebook](/notebooks/fuse.ipynb).

## Credits

The data and the works of CUHK-SYSU and CUHK-PEDES are from their respective authors :

- [CUHK-SYSU repo](https://github.com/ShuangLI59/person_search)
- [CUHK-PEDES repo](https://github.com/ShuangLI59/Person-Search-with-Natural-Language-Description)

```bibtex
@inproceedings{xiaoli2017joint,
  title={Joint Detection and Identification Feature Learning for Person Search},
  author={Xiao, Tong and Li, Shuang and Wang, Bochao and Lin, Liang and Wang, Xiaogang},
  booktitle={CVPR},
  year={2017}
}
@article{li2017person,
  title={Person search with natural language description},
  author={Li, Shuang and Xiao, Tong and Li, Hongsheng and Zhou, Bolei and Yue, Dayu and Wang, Xiaogang},
  journal={arXiv preprint arXiv:1702.05729},
  year={2017}
}
```

The COCO format annotations in `.json` is adapted from the [AlignPS repo](https://github.com/daodaofr/AlignPS).

```bibtex
@inproceedings{DBLP:conf/cvpr/YanLQBL00021,
  author    = {Yichao Yan and
               Jinpeng Li and
               Jie Qin and
               Song Bai and
               Shengcai Liao and
               Li Liu and
               Fan Zhu and
               Ling Shao},
  title     = {Anchor-Free Person Search},
  booktitle = {{IEEE} Conference on Computer Vision and Pattern Recognition},
  pages     = {7690--7699},
  year      = {2021}
}

@article{DBLP:journals/corr/abs-2109-00211,
  author    = {Yichao Yan and
               Jinpeng Li and
               Jie Qin and
               Shengcai Liao and
               Xiaokang Yang},
  title     = {Efficient Person Search: An Anchor-Free Approach},
  journal   = {CoRR},
  volume    = {abs/2109.00211},
  year      = {2021}
}
```

Thanks to the [AURA funding](https://www.auvergnerhonealpes.fr/contenus/ma-subvention-regles-et-visibilite-logo) for financing my PhD works which includes this repo.
