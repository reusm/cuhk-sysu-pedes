from pathlib import Path

from scipy.io import loadmat
import pandas as pd
from pandas.core.frame import DataFrame
import pytest

from cuhk_sysu_pedes.merge import merge_datasets
from cuhk_sysu_pedes.sysu_annotations import (
    build_sysu_annotations_from_file,
)
from cuhk_sysu_pedes.pedes_annotations import (
    build_pedes_annotations_from_file,
)


@pytest.fixture(scope="package")
def cuhk_sysu_dataset_folder_path():
    return "./data/CUHK-SYSU"


@pytest.fixture(scope="package")
def cuhk_pedes_dataset_folder_path():
    return "./data/CUHK-PEDES"


@pytest.fixture(scope="package")
def sysu_annotations(cuhk_sysu_dataset_folder_path):
    return build_sysu_annotations_from_file(Path(cuhk_sysu_dataset_folder_path))


@pytest.fixture(scope="package")
def pedes_annotations(cuhk_pedes_dataset_folder_path):
    return build_pedes_annotations_from_file(Path(cuhk_pedes_dataset_folder_path))


@pytest.fixture(scope="package")
def annotations(sysu_annotations: DataFrame, pedes_annotations: DataFrame):
    return merge_datasets(pedes_annotations, sysu_annotations)


@pytest.fixture
def total_number_person_ids():
    return 8_432


@pytest.fixture
def samples_number():
    return dict(
        sysu=348_166,
        merged=126_585,
        pedes=23_421,
    )


@pytest.fixture
def distractor_number():

    return {
        "sysu": 284_559,
        "merged": 62_978,
    }


@pytest.fixture
def matlab_sysu_test_annotations():
    _matlab_sysu_test_annotations = loadmat(
        "data/CUHK-SYSU/annotation/test/train_test/TestG100.mat"
    )["TestG100"]

    return _matlab_sysu_test_annotations


@pytest.fixture
def unlabeled_train_sysu_detections_number():
    return 40_186


@pytest.fixture
def labeled_train_sysu_detections_number():
    return 15_080


@pytest.fixture
def split_values():
    return dict(
        sysu=dict(
            train=55_266,
            query=2_900,
            gallery=100 * 2_900,
        ),
        merged=dict(
            train=61_844,
            query=641,
            gallery=100 * 641,
        ),
        pedes=dict(
            train=19_892,
            test=1_763,
            val=1_766,
        ),
    )


@pytest.fixture
def get_distractors():

    def _get_distractors(anns: pd.DataFrame):
        return anns[anns.bbox_x.isna()]

    return _get_distractors


@pytest.fixture
def get_number_person_ids():

    def _number_person_ids(anns: pd.DataFrame):
        return len(anns.index.get_level_values("person_id").unique())

    return _number_person_ids


@pytest.fixture
def get_split_sysu():
    # split in ("train", "query", "gallery")
    def _get_split_sysu(anns: pd.DataFrame, split: str):
        return anns[anns.split_sysu == split]

    return _get_split_sysu


@pytest.fixture
def get_split_pedes():
    # split in ("train", "test", "val")
    def _get_split_pedes(anns: pd.DataFrame, split: str):
        return anns[anns.split_pedes == split]

    return _get_split_pedes


@pytest.fixture
def get_split_merged():
    # split in ("train", "test")
    def _get_split_merged(anns: pd.DataFrame, split: str):
        return anns[anns.split == split]

    return _get_split_merged


@pytest.fixture
def get_labeled_detections():

    def _get_labeled_detections(anns: pd.DataFrame):
        train_sysu = anns.query("split_sysu == 'train'")
        return train_sysu[train_sysu.index.get_level_values("person_id") > -1]

    return _get_labeled_detections


@pytest.fixture
def get_unlabeled_detections():

    def _get_unlabeled_detections(anns: pd.DataFrame):
        train_sysu = anns.query("split_sysu == 'train'")
        return train_sysu[train_sysu.index.get_level_values("person_id") == -1]

    return _get_unlabeled_detections


@pytest.fixture
def count_rows_with_na():

    def _count_rows_with_na(anns: pd.DataFrame):
        return anns.isna().any(axis=1).sum()

    return _count_rows_with_na


@pytest.fixture
def csv_number_lines():
    return 348_197


@pytest.fixture
def stats_open_mmlab():
    return {
        "train": {
            "number_frames": 17_548,
            "number_annotations": 61_844,
            "number_persons": 7_792,
        }
    }
