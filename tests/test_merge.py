import pandas as pd


def test_no_overlap_between_splits(annotations: pd.DataFrame):
    train_person_ids = (
        annotations \
        .query("split == 'train'") \
        .index \
        .get_level_values("person_id") \
    )
    test_person_ids = (
        annotations \
        .query("split != 'train'") \
        .index \
        .get_level_values("person_id") \
    )

    assert len(train_person_ids.intersection(test_person_ids)) == 0


def test_samples_test_size_101(annotations: pd.DataFrame):
    assert (
        annotations \
        .query("split != 'train'") \
        .groupby("person_id") \
        .apply(lambda s: len(s) == 101) \
        .all() \
    )


def test_test_reid_annotations(
    pedes_annotations: pd.DataFrame, annotations: pd.DataFrame
):
    n_pedes_test = len(pedes_annotations.query("split_pedes == 'test'"))
    n_test = len(annotations.dropna().query("split != 'train'"))

    assert n_pedes_test == n_test


def test_test_reid_ids(pedes_annotations: pd.DataFrame, annotations: pd.DataFrame):
    n_pedes_test_ids = len(
        pedes_annotations.query("split_pedes == 'test'").groupby("person_id")
    )
    n_test_ids = len(
        annotations.dropna().query("split != 'train'").groupby("person_id")
    )

    assert n_pedes_test_ids == n_test_ids


def test_train_reid_ids(pedes_annotations: pd.DataFrame, annotations: pd.DataFrame):
    n_pedes_train_val_ids = len(
        pedes_annotations.query("split_pedes in ('train', 'val')").groupby("person_id")
    )
    n_train_ids = len(
        annotations.dropna().query("split == 'train'").groupby("person_id")
    )

    assert n_pedes_train_val_ids == n_train_ids


def test_train_reid_annotations(
    pedes_annotations: pd.DataFrame, annotations: pd.DataFrame
):
    n_pedes_train_val = len(pedes_annotations.query("split_pedes in ('train', 'val')"))
    n_train = len(annotations.dropna().query("split == 'train'"))

    assert n_pedes_train_val == n_train


def test_train_detections(sysu_annotations: pd.DataFrame, annotations: pd.DataFrame):

    n_sysu_train_detections = (
        sysu_annotations.index.get_level_values("person_id") == -1
    ).sum()
    n_annotations_train_detections = (
        annotations.index.get_level_values("person_id") == -1
    ).sum()

    assert n_sysu_train_detections == n_annotations_train_detections
