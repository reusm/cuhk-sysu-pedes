import argparse
import pytest
from cuhk_sysu_pedes.cli import (
    _set_parser,
)
from cuhk_sysu_pedes import cli
from cuhk_sysu_pedes.constants import FORMATS


class TestParserInput:

    def test_parser_wrong_pedes_folder(self, mocker):
        mocked_args = argparse.Namespace(
            sysu_path="data/CUHK-SYSU",
            pedes_path="data/CUHK-PEdES",
            formats=["mmlab"],
            output_folder="./outputs",
            generation_mode="dataset",
        )
        mocker.patch(
            "argparse.ArgumentParser.parse_args",
            return_value=mocked_args,
        )

        with pytest.raises(FileNotFoundError):
            cli.main()

    def test_parser_wrong_sysu_folder(self, mocker):
        mocked_args = argparse.Namespace(
            sysu_path="data/CUHK-SYSu",
            pedes_path="data/CUHK-PEDES",
            formats=["csv"],
            output_folder="./outputs",
            generation_mode="dataset",
        )
        mocker.patch(
            "argparse.ArgumentParser.parse_args",
            return_value=mocked_args,
        )

        with pytest.raises(FileNotFoundError):
            cli.main()


class TestParser:

    @pytest.fixture
    def parser_cli(self):
        return _set_parser()

    def test_parser_without_args(self, parser_cli):
        """
        User passes no args, should fail with SystemExit
        """
        # No arguments should raise a SystemExit
        with pytest.raises(SystemExit):
            parser_cli.parse_args([])

    def test_parser_default_args(self, parser_cli):
        """
        The parser should not exit if it receives required arguments
        """
        # Args are: sysu_path and pedes_path
        args = parser_cli.parse_args(["/path/to/sysu", "/path/to/pedes"])

        assert args.sysu_path == "/path/to/sysu"
        assert args.pedes_path == "/path/to/pedes"
        assert args.formats == "mmlab"  # default
        assert args.output_folder == "."  # default
        assert args.generation_modes == "dataset"  # default

    def test_parser_formats(self, parser_cli):
        args = parser_cli.parse_args(
            [
                "/path/to/sysu",
                "/path/to/pedes",
                "--formats",
                "csv",
                "mmlab",
            ]
        )

        assert args.formats == ["csv", "mmlab"]

    def test_parser_formats_all(self, parser_cli):
        # Check all possible choices
        for choice in FORMATS:
            args = parser_cli.parse_args(
                [
                    "/path/to/sysu",
                    "/path/to/pedes",
                    "--formats",
                    choice,
                ]
            )
            assert args.formats == [choice]

    def test_parser_formats_wrong(self, parser_cli):
        # Invalid choice should raise an error
        invalid_format = "invalid_choice"
        with pytest.raises(SystemExit):
            parser_cli.parse_args(
                [
                    "/path/to/sysu",
                    "/path/to/pedes",
                    "--formats",
                    invalid_format,
                ]
            )

    def test_parser_output_folder(self, parser_cli):
        """
        The parser should handle the output_folder option correctly
        """
        # Args are: sysu_path, pedes_path and output_folder
        output_folder = "/output/path"
        args = parser_cli.parse_args(
            [
                "/path/to/sysu",
                "/path/to/pedes",
                "--output-folder",
                output_folder,
            ]
        )

        assert args.output_folder == output_folder
