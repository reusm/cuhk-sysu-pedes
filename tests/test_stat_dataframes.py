def test_person_ids_number(
    sysu_annotations,
    pedes_annotations,
    annotations,
    total_number_person_ids,
    get_number_person_ids,
):
    assert (get_number_person_ids(pedes_annotations)) == total_number_person_ids
    assert (
        # -1 for 'person_id=-1'  unlabeled detections
        get_number_person_ids(sysu_annotations) - 1
    ) == total_number_person_ids
    assert (
        # -1 for 'person_id=-1' unlabeled detections
        get_number_person_ids(annotations) - 1
    ) == total_number_person_ids


def test_unlabeled_detections_duplicate_sysu(sysu_annotations):
    """See issue #18"""
    train_annotations_groupedby_frame_id = sysu_annotations.query(
        "split_sysu == 'train'"
    ).groupby("frame_id")
    dfs_train_annotations_groupedby_frame_id = [
        df for _, df in train_annotations_groupedby_frame_id
    ]

    assert not any(
        df.duplicated(subset=["bbox_x", "bbox_y", "bbox_w", "bbox_h"]).sum() > 0
        for df in dfs_train_annotations_groupedby_frame_id
    )


def test_sample_numbers(
    sysu_annotations,
    pedes_annotations,
    annotations,
    samples_number,
):
    assert len(sysu_annotations) == samples_number["sysu"]
    assert len(annotations) == samples_number["merged"]
    assert len(pedes_annotations) == samples_number["pedes"]


# Check the number of distractor in sysu_split
# Check that distractor has sysu_split == 'gallery'
def test_distractor_numbers(
    sysu_annotations,
    annotations,
    distractor_number,
    get_distractors,
):
    sysu_distractors = get_distractors(sysu_annotations)
    merged_distractors = get_distractors(annotations)

    assert len(sysu_distractors) == distractor_number["sysu"]
    assert len(merged_distractors) == distractor_number["merged"]

    assert (sysu_distractors.split_sysu == "gallery").all()
    assert (merged_distractors.split == "gallery").all()


def test_detections_unlabeled(
    sysu_annotations,
    annotations,
    get_unlabeled_detections,
    unlabeled_train_sysu_detections_number,
):
    assert (
        len(get_unlabeled_detections(sysu_annotations)
            ) == unlabeled_train_sysu_detections_number
    )
    assert (
        len(get_unlabeled_detections(annotations)
            ) == unlabeled_train_sysu_detections_number
    )


def test_detections_labeled(
    sysu_annotations,
    annotations,
    get_labeled_detections,
    labeled_train_sysu_detections_number,
):
    assert (
        len(get_labeled_detections(sysu_annotations)
            ) == labeled_train_sysu_detections_number
    )
    assert (
        len(get_labeled_detections(annotations)) == labeled_train_sysu_detections_number
    )


def test_nan_values(
    sysu_annotations,
    annotations,
    distractor_number,
    unlabeled_train_sysu_detections_number,
    count_rows_with_na,
):
    # Distractor and train unlabeled has nan
    assert count_rows_with_na(sysu_annotations) == distractor_number["sysu"]
    assert count_rows_with_na(annotations) == (
        distractor_number["merged"]
        # Unlabeled detection does not have PEDES annotations = NA values
        + unlabeled_train_sysu_detections_number
    )


def test_split(
    sysu_annotations,
    pedes_annotations,
    annotations,
    get_split_pedes,
    get_split_sysu,
    get_split_merged,
    split_values,
):
    assert (
        len(get_split_sysu(sysu_annotations, "train")) == split_values["sysu"]["train"]
    )
    assert (
        len(get_split_sysu(sysu_annotations, "query")) == split_values["sysu"]["query"]
    )
    assert (
        len(get_split_sysu(sysu_annotations,
                           "gallery")) == split_values["sysu"]["gallery"]
    )

    assert (
        len(get_split_pedes(pedes_annotations,
                            "train")) == split_values["pedes"]["train"]
    )
    assert (
        len(get_split_pedes(pedes_annotations, "val")) == split_values["pedes"]["val"]
    )
    assert (
        len(get_split_pedes(pedes_annotations, "test")) == split_values["pedes"]["test"]
    )

    assert (
        len(get_split_merged(annotations, "train")) == split_values["merged"]["train"]
    )
    assert (
        len(get_split_merged(annotations, "query")) == split_values["merged"]["query"]
    )
    assert (
        len(get_split_merged(annotations,
                             "gallery")) == split_values["merged"]["gallery"]
    )


def test_integrity(
    sysu_annotations,
    pedes_annotations,
):
    assert pedes_annotations.notna().all().all()

    sysu_annotations_without_gallery = sysu_annotations.query("split_sysu != 'gallery'")
    assert sysu_annotations_without_gallery.notna().all().all()
