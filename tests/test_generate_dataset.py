from pathlib import Path
import pytest

from cuhk_sysu_pedes.io.generate_openmmlab_dataset import generate_dataset


@pytest.fixture(scope="module")
def dataset_folder(
    annotations, cuhk_sysu_dataset_folder_path, tmp_path_factory
) -> Path:
    _dataset_folder = tmp_path_factory.mktemp("dataset")
    generate_dataset(annotations, _dataset_folder, cuhk_sysu_dataset_folder_path)

    return _dataset_folder


def test_subfolders(dataset_folder: Path):
    data_folder = dataset_folder / "data"
    assert data_folder.exists()

    annotations_folder = data_folder / "annotations"
    assert annotations_folder.exists()

    train_folder = data_folder / "train"
    assert train_folder.exists()


def test_number_images(dataset_folder: Path, stats_open_mmlab):
    split_to_folders = {
        split: dataset_folder / "data" / split
        for split in stats_open_mmlab
    }

    for split, folder in split_to_folders.items():
        number_frames = len(
            [file for file in folder.iterdir() if file.suffix == ".jpg"]
        )

        assert stats_open_mmlab[split]["number_frames"] == number_frames
