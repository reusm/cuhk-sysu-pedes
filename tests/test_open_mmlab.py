import pytest
import pandas as pd

from cuhk_sysu_pedes.io.open_mmlab import (
    serialize_annotations_to_mmlab_format,
)


@pytest.fixture
def serialized_annotations(annotations: pd.DataFrame):
    return serialize_annotations_to_mmlab_format(annotations)


def test_format_structure(serialized_annotations, stats_open_mmlab):
    metainfo = serialized_annotations["metainfo"]
    split = metainfo["split"]

    data_list = serialized_annotations["data_list"]

    number_frames = len(data_list)
    number_annotations = sum(len(data["detection_annotations"]) for data in data_list)
    number_persons = metainfo["number_person_ids"]

    assert number_frames == stats_open_mmlab[split]["number_frames"]
    assert number_annotations == stats_open_mmlab[split]["number_annotations"]
    assert number_persons == stats_open_mmlab[split]["number_persons"]
