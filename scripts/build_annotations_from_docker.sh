# $1 is the target path for the outputs folder with annotations inside it
docker volume rm cuhk_sysu_pedes_codebase_outputs
docker compose up --build test
container_id=$(docker compose ps -q --all test)
exit_code=$(docker inspect $container_id --format='{{.State.ExitCode}}')

if [ "$exit_code" -ne 0 ]; then
	docker logs $container_id
	echo -e "\033[31m"
	echo "Test failed, cannot build the annotations"
	echo "Check the .env file is filled correctly."
	echo -e "\033[0"
	exit 1
fi

echo -e "\033[32mTest passed!\033[0m"

docker compose up --build create
docker compose up -d retrieve
docker compose cp retrieve:/outputs $1

docker compose down
