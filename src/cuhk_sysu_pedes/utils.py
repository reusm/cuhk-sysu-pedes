"""The module implement handy functions that are used internally"""


from typing import Any


def print_result(
    title: str,
    lines: list[tuple[str, Any]],
    line_space: int = 35,
    title_space: int = 55,
):
    print(f"{' ' + title + ' ':~^{title_space}s}")
    for description, value in lines:
        print(f"{description:>{line_space}s} {value}")
    print(f"{' x ':~^{title_space}s}")
