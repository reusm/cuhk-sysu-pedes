"""
    Produce the stats for database.
"""
from functools import reduce

import pandas as pd

from cuhk_sysu_pedes import pedes_annotations


def counting_detections(annotations: pd.DataFrame) -> int:
    """Counting all row in annotations dataframe"""
    return annotations.index.size


def counting_frames(annotations: pd.DataFrame) -> int:
    """Counting all unique row based on frame IDs"""
    return annotations.index.unique(level="frame_id").size


def counting_persons(annotations: pd.DataFrame) -> int:
    """Counting all unique row based on person IDs"""
    return annotations.index.unique(level="person_id").size


def counting_detections_by_person(annotations: pd.DataFrame) -> dict:
    # Stat detections by persons

    n_detections_by_person: pd.Series = annotations.index.get_level_values(
        "person_id"
    ).value_counts()

    # Number of person for normalisation for proportion quantities
    n_persons = counting_persons(annotations)

    return {
        "COUNT": n_detections_by_person,
        "STAT": {
            "MEAN": (mean := n_detections_by_person.mean()),
            "STD": (std := n_detections_by_person.std()),
            "CENTERED 1 STD": {
                "COUNT": (
                    count_within_1_std_centered_inteverval := n_detections_by_person.between(
                        mean - std,
                        mean + std,
                    ).sum()
                ),
                "PROPORTION": count_within_1_std_centered_inteverval.sum() / n_persons,
            },
        },
        "EQUALS TO": (
            count_equals_from_2_to_4 := {
                i: {
                    "COUNT": (count_equal_to_i := (n_detections_by_person == i).sum()),
                    "PROPORTION": count_equal_to_i / n_persons,
                }
                for i in range(2, 4 + 1)
            }
        ),
        "PROPORTION 4 AND BELOW": (
            sum(count_equals_from_2_to_4[i]["COUNT"] for i in range(2, 4 + 1))
            / n_persons
        ),
    }


def counting_detections_by_frame(annotations: pd.DataFrame) -> dict:
    # Stat detections by frame
    n_detections_by_frame: pd.Series = annotations.index.get_level_values(
        "frame_id"
    ).value_counts()

    # Number of frames for normalisation for proportion quantities
    n_frames = counting_frames(annotations)

    return {
        "COUNT": n_detections_by_frame,
        "STAT": {
            "MEAN": (mean := n_detections_by_frame.mean()),
            "STD": (std := n_detections_by_frame.std()),
            "CENTERED 1 STD": {
                "COUNT": (
                    count_within_1_std_centered_inteverval := n_detections_by_frame.between(
                        mean - std,
                        mean + std,
                    ).sum()
                ),
                "PROPORTION": count_within_1_std_centered_inteverval.sum() / n_frames,
            },
        },
        "EQUALS TO": (
            count_equals_from_1_to_3 := {
                i: {
                    "COUNT": (count_equal_to_i := (n_detections_by_frame == i).sum()),
                    "PROPORTION": count_equal_to_i / n_frames,
                }
                for i in range(1, 3 + 1)
            }
        ),
        "PROPORTION 3 AND BELOW": (
            sum(count_equals_from_1_to_3[i]["COUNT"] for i in range(1, 3 + 1))
            / n_frames
        ),
    }


def are_split_intersecting(
    annotations_train: pd.DataFrame, annotations_test: pd.DataFrame
) -> bool:
    # Build set of index and check their intersection is empty.
    set_of_index_annotations_train = set(annotations_train.index.to_list())
    set_of_index_annotations_test = set(annotations_test.index.to_list())

    return bool(
        set_of_index_annotations_train.intersection(set_of_index_annotations_test)
    )


def verify_distractors(annotations_test: pd.DataFrame) -> dict:
    """
    Set of verifications on distractor e.g gallery frames which are not annotated, they do not
    contain the person in the query.

    - There is no missing annotations in the query
    - There is no (person ID, frame ID) indexes overlap between annotated detections and distractors
    - Count distractors' frames number : Total and unique frames
    """
    distractors = annotations_test[annotations_test.isna().any(axis=1)]
    annotations_annotated = annotations_test.dropna()
    query_annotations = annotations_test[annotations_test.split_sysu == "query"]

    return {
        "DISTRACTOR IN QUERY": query_annotations.isna().any(axis=1).any(),
        "COUNT FRAME": {
            "TOTAL": distractors.index.size,
            "UNIQUE": distractors.index.unique("frame_id").size,
        },
        "HAVE COMMON INDEX": not pd.Index.intersection(
            annotations_annotated.index,
            distractors.index,
        ).empty,
    }


def query_exists(annotations_test: pd.DataFrame) -> bool:
    return all(
        len(sample[1].query("split_sysu == 'query'"))  # index 1 is the dataframe object
        == 1
        for sample in annotations_test.groupby("person_id")
    )


def galleries_have_100_detections(annotations_test: pd.DataFrame) -> bool:
    count_detections_in_gallery = (
        # Only gallery annotations
        annotations_test.query(
            f"split_sysu == 'gallery'",
        )
        # Count detections by person_id
        .groupby("person_id").apply(len)
        # Convert result to numer
        .to_numpy()
    )

    return count_detections_in_gallery[count_detections_in_gallery == 100].all()


def are_missing_captions(annotations: pd.DataFrame) -> bool:
    return annotations[pedes_annotations.COLUMN_NAMES].isna().values.any()


def count_words(annotations: pd.DataFrame) -> dict:
    # We make a dictionnary for mapping caption 1 to his series, same with
    # caption 2 and both caption.
    number_captions_subsets = {
        "CAPTION 1 AND 2": pd.concat(
            annotations.reset_index()[col] for col in pedes_annotations.COLUMN_NAMES[1:]
        ),
        "CAPTION 1": annotations.reset_index()[pedes_annotations.COLUMN_NAMES[1]],
        "CAPTION 2": annotations.reset_index()[pedes_annotations.COLUMN_NAMES[2]],
    }

    return {
        caption_subset_name: {
            "CAPTIONS COUNT": captions_subset.index.size,
            "WORDS STAT": _get_words_stat(captions_subset),
        }
        for caption_subset_name, captions_subset in number_captions_subsets.items()
    }


def _standardized_captions(captions: pd.Series) -> pd.Series:
    return captions.str.lower().str.replace(r"[;:,.]?", "", regex=True)


def _build_word_count(captions: pd.Series) -> pd.DataFrame:
    return (
        # Split by whitespace and do one columns for each word
        captions.str.split(expand=True)
        .stack()  # The index are multi from previous expanding, it unpacks the dimensions
        .value_counts()  # Count each occurence of the word
    )


def _get_words_stat(captions: pd.Series) -> pd.DataFrame:
    return reduce(
        lambda x, f: f(x), (captions, _standardized_captions, _build_word_count)
    )


def compute_captions_lengths(annotations: pd.DataFrame) -> dict:
    # Captions lengths dataframe
    captions_lengths = pd.Series(
        pd.concat(
            annotations[subset_caption]
            for subset_caption in pedes_annotations.COLUMN_NAMES[1:]
        )
        .sort_index()
        .str.split()
        .apply(len),
    )

    return {
        "STAT": captions_lengths,
        "MEAN": (mean := float(captions_lengths.mean())),
        "STD": (std := float(captions_lengths.std())),
        "INTERVALL CENTERED 1 STD": (mean - std, mean + std),
        "PROPORTION CENTERED 1 STD": (
            captions_lengths.between(mean - std, mean + std).sum()
            / captions_lengths.size
        ),
    }


def check_counting(annotations_1: pd.DataFrame, annotations_2: pd.DataFrame) -> bool:
    return all(
        counting_function(annotations_1) == counting_function(annotations_2)
        for counting_function in (
            counting_detections,
            counting_frames,
            counting_persons,
        )
    ) and all(
        (
            counting_function(annotations_1)["COUNT"].to_numpy()
            == counting_function(annotations_2)["COUNT"].to_numpy()
        ).all()
        for counting_function in (
            counting_detections_by_person,
            counting_detections_by_frame,
        )
    )


def distractors_are_ok(sysu_annotations_test, sysu_pedes_annotations_test) -> bool:
    sysu_count_distractors = verify_distractors(sysu_annotations_test)
    sysu_pedes_count_distractors = verify_distractors(sysu_pedes_annotations_test)

    return (
        all(
            not count_distractors[key]
            for key in ("DISTRACTOR IN QUERY", "HAVE COMMON INDEX")
            for count_distractors in (
                sysu_count_distractors,
                sysu_pedes_count_distractors,
            )
        )
        and sysu_count_distractors == sysu_pedes_count_distractors
    )
