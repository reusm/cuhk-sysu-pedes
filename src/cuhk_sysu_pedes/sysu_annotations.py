from pathlib import Path
from typing import TypeAlias

import numpy as np
import pandas as pd
import pkg_resources
from pandas._libs.missing import NAType
from scipy.io import loadmat

from cuhk_sysu_pedes.constants import INDEXES

SYSU_ANNOTATIONS_PATHS = {
    "FOLDER": Path("annotation", "test", "train_test"),
    "FILENAMES": {
        "TRAIN": "Train.mat",
        "TEST": "TestG100.mat",
    },
}

POOL_ANNOTATION_FILE = Path("annotation", "pool.mat")
IMAGES_ANNOTATION_FILE = Path("annotation", "Images.mat")

COLUMN_NAME_TO_DTYPE = {
    "split_sysu": "category",
    "is_hard": bool,
    "bbox_x": pd.UInt16Dtype(),
    "bbox_y": pd.UInt16Dtype(),
    "bbox_w": pd.UInt16Dtype(),
    "bbox_h": pd.UInt16Dtype(),
}

ANNOTATIONS_DATAFRAME = {
    "INDEX": INDEXES,
    "COLUMNS": list(COLUMN_NAME_TO_DTYPE.keys()),
    "NAME COLUMNS": [""],
}

SPLIT_CATEGORIES = ["train", "query", "gallery"]

BBOX_COORDINATE_TYPE: TypeAlias = pd.UInt16Dtype | NAType
ANNOTATION_TYPE_DATA: TypeAlias = tuple[
    str,
    bool,
    BBOX_COORDINATE_TYPE,
    BBOX_COORDINATE_TYPE,
    BBOX_COORDINATE_TYPE,
    BBOX_COORDINATE_TYPE,
]

PERSON_ID_UNLABELED_DETECTIONS = -1
# See issue #18
FRAME_ID_UNLABELED_DETECTIONS_WITH_DUPLICATES = 4940
NUMBER_TRAIN_SAMPLES_BEFORE_REMOVING_DUPLICATES = 15_088
NUMBER_TRAIN_SAMPLES_AFTER_REMOVING_DUPLICATES = 15_080
PERSON_IDS_DUPLICATED_WITH_DIFFERENT_ANNOTATIONS_IN_TRAIN = [
    1_688,
    3_560,
    6_967,
    8_022,
    11_039,
]

# See fix_duplicate_sysu.ipynb
REPLACEMENT_RECORDS_INFO_FILE = "data/replacements_record_info.npy"
DISTRACTOR_VALUE = (
    SPLIT_CATEGORIES[2],
    False,
    pd.NA,
    pd.NA,
    pd.NA,
    pd.NA,
)


def _import_replacement_mapping_info() -> dict[str, tuple[str, np.ndarray]]:
    file_path = pkg_resources.resource_filename(
        "cuhk_sysu_pedes", REPLACEMENT_RECORDS_INFO_FILE
    )

    replacement_records_info = np.load(file_path, allow_pickle=True)

    return {
        person_id: (frame_id, replacement_frame_annotation)
        for (
            person_id,
            frame_id,
            replacement_frame_annotation,
        ) in replacement_records_info
    }


def _build_sysu_annotations(
    matlab_annotations_train: np.ndarray,
    matlab_annotations_test: np.ndarray,
    matlab_annotations_pool: np.ndarray,
    matlab_annotations_all_images: np.ndarray,
) -> pd.DataFrame:
    all_train_annotations = _build_sysu_annotations_train(
        matlab_annotations_train,
        matlab_annotations_pool,
        matlab_annotations_all_images,
    )
    test_annotations = _build_sysu_annotations_test(matlab_annotations_test)

    annotations = pd.concat([all_train_annotations, test_annotations]).astype(
        COLUMN_NAME_TO_DTYPE
    )

    return annotations


def _get_person_id_from_mat_record(
    mat_record: np.ndarray,
) -> int:
    # ["idname"][0][0][0] accesses the str
    # [1:] -> remove the p in front of person ID
    return int(mat_record["idname"][0][0][0][1:])


def _extract_frame_id_from_str(frame_id_str: str) -> int:
    return int(frame_id_str.split(".")[0][1:])


def _get_frame_id_from_mat_record(
    mat_record: np.ndarray,
) -> int:
    # Should adjust str extraction because all matlab records does not have the
    # same nested structure
    frame_id_str = (
        mat_record["imname"][0]
        if isinstance(mat_record["imname"][0], str)
        else mat_record["imname"][0][0][0]
    )
    return _extract_frame_id_from_str(frame_id_str)


def _get_bbox_from_mat_record(
    mat_record: np.ndarray,
) -> tuple[pd.UInt16Dtype, pd.UInt16Dtype, pd.UInt16Dtype, pd.UInt16Dtype,]:
    # Nested structure is not the same based that the matlab record comes from
    # a train or a test annotation.
    bbox = (
        mat_record["idlocate"][0]
        if not isinstance(mat_record["idlocate"][0][0], np.ndarray)
        else mat_record["idlocate"][0][0][0]
    )
    return (
        bbox.astype(pd.UInt16Dtype)[0],
        bbox.astype(pd.UInt16Dtype)[1],
        bbox.astype(pd.UInt16Dtype)[2],
        bbox.astype(pd.UInt16Dtype)[3],
    )


def _remove_duplicate_train_annotations(
    train_annotations: pd.DataFrame,
) -> pd.DataFrame:
    assert (
        len(train_annotations) == NUMBER_TRAIN_SAMPLES_BEFORE_REMOVING_DUPLICATES
    ), "Wrong SYSU train annotations before fixing duplicates"

    # Control the identity of duplicate.
    duplicated_dfs = [df for _, df in train_annotations.groupby(INDEXES) if len(df) > 1]
    for duplicate_df in duplicated_dfs:
        person_id = duplicate_df.index.get_level_values("person_id").values[0]

        (_, sample_1), (_, sample_2) = duplicate_df.iterrows()
        assert (
            sample_1.equals(sample_2)
            or person_id in PERSON_IDS_DUPLICATED_WITH_DIFFERENT_ANNOTATIONS_IN_TRAIN
        ), "Tried to unknown duplicates"

    train_annotations_without_duplicates = train_annotations.groupby(INDEXES).first()

    # Control the number of duplicates that should be removed.
    assert (
        len(train_annotations_without_duplicates)
        == NUMBER_TRAIN_SAMPLES_AFTER_REMOVING_DUPLICATES
    )

    return train_annotations_without_duplicates


def _build_sysu_annotations_train_data(
    detection_annotation: np.ndarray,
) -> tuple[int, ANNOTATION_TYPE_DATA]:
    frame_id = _get_frame_id_from_mat_record(detection_annotation)

    data = (
        SPLIT_CATEGORIES[0],
        bool(detection_annotation["ishard"][0][0]),
        *_get_bbox_from_mat_record(detection_annotation),
    )

    return frame_id, data


def _get_train_image_names(
    matlab_annotations_pool: np.ndarray,
    matlab_annotations_images: np.ndarray,
) -> list[np.ndarray]:
    test_image_names = [
        str(image_name[0]) for image_name in matlab_annotations_pool[:, 0]
    ]
    all_image_names = [
        str(image["imname"][0]) for image in matlab_annotations_images[0]
    ]
    train_image_names = set(all_image_names) - set(test_image_names)
    train_images_mat = [
        image
        for image in matlab_annotations_images[0]
        if str(image["imname"][0]) in train_image_names
    ]

    return train_images_mat


def _get_labeled_image_name_and_bboxes(
    annotated_detections: pd.DataFrame,
) -> list[tuple[str, tuple[int, ...]]]:
    annotated_frame_ids = annotated_detections.reset_index().frame_id.values
    annotated_bboxes = annotated_detections.reset_index()[
        ["bbox_x", "bbox_y", "bbox_w", "bbox_h"]
    ].values

    annotated_frame_ids_and_detections = [
        (frame_id, tuple(detection))
        for frame_id, detection in zip(annotated_frame_ids, annotated_bboxes)
    ]
    assert len(annotated_frame_ids_and_detections) == len(
        set(annotated_frame_ids_and_detections)
    )

    annotated_image_names_and_detections = [
        (f"s{frame_id}.jpg", detection)
        for frame_id, detection in annotated_frame_ids_and_detections
    ]

    return annotated_image_names_and_detections


def _fix_unlabeled_detections_duplicates(
    unlabeled_detections: pd.DataFrame,
) -> pd.DataFrame:
    """See issue #18."""
    duplicates_index = (
        PERSON_ID_UNLABELED_DETECTIONS,
        FRAME_ID_UNLABELED_DETECTIONS_WITH_DUPLICATES,
    )

    fixed_duplicate_detections = unlabeled_detections.loc[
        duplicates_index, :
    ].drop_duplicates()

    unlabeled_detection_without_duplicates = pd.concat(
        [unlabeled_detections.drop(index=duplicates_index), fixed_duplicate_detections]
    )

    return unlabeled_detection_without_duplicates


def _build_unlabeled_detections(
    annotated_detections: pd.DataFrame,
    matlab_annotations_pool: np.ndarray,
    matlab_annotations_images: np.ndarray,
) -> pd.DataFrame:
    train_images_mat = _get_train_image_names(
        matlab_annotations_pool, matlab_annotations_images
    )
    annotated_image_names_and_detections = _get_labeled_image_name_and_bboxes(
        annotated_detections
    )

    unlabeled_detections_data = {
        "index": [],
        "data": [],
        "columns": ANNOTATIONS_DATAFRAME["COLUMNS"],
        "index_names": ANNOTATIONS_DATAFRAME["INDEX"],
        "column_names": ANNOTATIONS_DATAFRAME["NAME COLUMNS"],
    }

    for image in train_images_mat:
        image_name = str(image["imname"].squeeze())
        for bbox in image["box"][0]:
            image_name_and_bbox = (
                image_name,
                tuple(bbox[0][0]),
            )
            # Skip labeled detections
            detection_is_annotated = (
                image_name_and_bbox in annotated_image_names_and_detections
            )
            if detection_is_annotated:
                continue

            index = (
                PERSON_ID_UNLABELED_DETECTIONS,
                _extract_frame_id_from_str(image_name),
            )
            unlabeled_detections_data["index"].append(index)

            is_hard = int(bbox["ishard"].squeeze())
            bbox = map(int, image_name_and_bbox[1])
            # bbox = tuple(
            #     int(coordinate)
            #     for coordinate in image_name_and_bbox[1]
            # )
            data = (SPLIT_CATEGORIES[0], is_hard, *bbox)
            unlabeled_detections_data["data"].append(data)

    unlabeled_detections = pd.DataFrame.from_dict(
        unlabeled_detections_data, orient="tight"
    ).sort_index()

    # See issue #18
    unlabeled_detection_without_duplicates = _fix_unlabeled_detections_duplicates(
        unlabeled_detections
    )

    return unlabeled_detection_without_duplicates


def _build_sysu_annotations_train(
    matlab_annotations_train: np.ndarray,
    matlab_annotations_pool: np.ndarray,
    matlab_annotations_all_images: np.ndarray,
) -> pd.DataFrame:
    # Dict for building dataframe object
    annotations_data = {
        "index": [],
        "data": [],
        "columns": ANNOTATIONS_DATAFRAME["COLUMNS"],
        "index_names": ANNOTATIONS_DATAFRAME["INDEX"],
        "column_names": ANNOTATIONS_DATAFRAME["NAME COLUMNS"],
    }

    for person_annotation in matlab_annotations_train:
        person_id = _get_person_id_from_mat_record(person_annotation[0])

        frame_annotations = person_annotation[0]["scene"][0][0][0]
        for detection_annotation in frame_annotations:
            (
                frame_id,
                data,
            ) = _build_sysu_annotations_train_data(detection_annotation)

            annotations_data["index"].append((person_id, frame_id))
            annotations_data["data"].append(data)

    annotated_detections = _remove_duplicate_train_annotations(
        pd.DataFrame.from_dict(annotations_data, orient="tight")
    )

    unlabeled_detections = _build_unlabeled_detections(
        annotated_detections,
        matlab_annotations_pool,
        matlab_annotations_all_images,
    )

    all_train_detections = pd.concat([annotated_detections, unlabeled_detections])

    return all_train_detections


def _build_sysu_annotations_test_query_data(
    query_annotation: np.ndarray,
) -> tuple[tuple[int, int], ANNOTATION_TYPE_DATA]:
    query_index = (
        _get_person_id_from_mat_record(query_annotation),
        _get_frame_id_from_mat_record(query_annotation),
    )

    #  (type=query, is_hard, bbox_x, bbox_y, bbox_w, bbox_h)
    query_data = (
        SPLIT_CATEGORIES[1],
        bool(query_annotation["ishard"][0][0][0][0]),
        *_get_bbox_from_mat_record(query_annotation),
    )
    return query_index, query_data


def _build_sysu_annotations_test_gallery_element_data(
    frame_annotation: np.ndarray,
) -> tuple[int, ANNOTATION_TYPE_DATA]:
    frame_id = _get_frame_id_from_mat_record(frame_annotation)
    # Verify if the annotation is from distractor detection
    is_distractor = not (frame_annotation["idlocate"].any())
    if is_distractor:
        # Empty values for distractor
        return (frame_id, DISTRACTOR_VALUE)
        #  (type=gallery, is_hard, bbox_x, bbox_y, bbox_w, bbox_h)

    data = (
        SPLIT_CATEGORIES[2],
        bool(frame_annotation["ishard"][0].sum()),
        *_get_bbox_from_mat_record(frame_annotation),
    )
    return frame_id, data


def _fix_gallery_from_error(
    gallery: np.ndarray,
    frame_id_to_replace: str,
    frame_annotation_replacement: np.ndarray,
) -> np.ndarray:
    for i, frame_annotation in enumerate(gallery):
        if frame_id_to_replace == frame_annotation["imname"][0]:
            gallery[i] = frame_annotation_replacement.copy()
            return gallery

    raise RuntimeError("Corrupted gallery could not be fixed.")


def _get_gallery(
    frame_id_str: str,
    replacement_mapping_info: dict[str, tuple[str, np.ndarray]],
    gallery_annotation: np.ndarray,
) -> np.ndarray:
    gallery_has_problem = frame_id_str in replacement_mapping_info.keys()

    if not gallery_has_problem:
        return gallery_annotation

    (
        frame_id_to_replace,
        frame_annotation_replacement,
    ) = replacement_mapping_info[frame_id_str]
    return _fix_gallery_from_error(
        gallery_annotation,
        frame_id_to_replace,
        frame_annotation_replacement,
    )


def _build_sysu_annotations_test(
    matlab_annotations_test: np.ndarray,
) -> pd.DataFrame:
    # Dict for building dataframe object
    annotations_data = {
        "index": [],
        "data": [],
        "columns": ANNOTATIONS_DATAFRAME["COLUMNS"],
        "index_names": ANNOTATIONS_DATAFRAME["INDEX"],
        "column_names": ANNOTATIONS_DATAFRAME["NAME COLUMNS"],
    }

    queries_annotation = matlab_annotations_test["Query"][0]
    for query_annotation in queries_annotation:
        (
            query_index,
            query_data,
        ) = _build_sysu_annotations_test_query_data(
            query_annotation,
        )
        annotations_data["index"].append(query_index)
        annotations_data["data"].append(query_data)

    # Load replacements annotations for bad row
    # sample_person_id_concerned -> (frame_id_to_replace, frame_annotation)
    replacement_mapping_info = _import_replacement_mapping_info()
    galleries_annotation = matlab_annotations_test["Gallery"][0]
    # Gallery annotations
    for query_annotation, gallery_annotation in zip(
        queries_annotation, galleries_annotation
    ):
        query_person_id = _get_person_id_from_mat_record(query_annotation)

        # See fix notebook
        checked_gallery = _get_gallery(
            query_annotation["idname"][0][0][0],
            replacement_mapping_info,
            gallery_annotation[0],
        )

        for frame_annotation in checked_gallery:
            (
                frame_id,
                data,
            ) = _build_sysu_annotations_test_gallery_element_data(frame_annotation)

            annotations_data["index"].append((query_person_id, frame_id))
            annotations_data["data"].append(data)

    return pd.DataFrame.from_dict(annotations_data, orient="tight")


def _open_sysu_annotations(
    dataset_folder: Path,
) -> tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    matlab_annotations_train = loadmat(
        dataset_folder
        / SYSU_ANNOTATIONS_PATHS["FOLDER"]
        / SYSU_ANNOTATIONS_PATHS["FILENAMES"]["TRAIN"]
    )["Train"]
    matlab_annotations_test = loadmat(
        dataset_folder
        / SYSU_ANNOTATIONS_PATHS["FOLDER"]
        / SYSU_ANNOTATIONS_PATHS["FILENAMES"]["TEST"]
    )["TestG100"]
    matlab_annotations_pool = loadmat(dataset_folder / POOL_ANNOTATION_FILE)["pool"]
    matlab_annotations_images = loadmat(dataset_folder / IMAGES_ANNOTATION_FILE)["Img"]
    return (
        matlab_annotations_train,
        matlab_annotations_test,
        matlab_annotations_pool,
        matlab_annotations_images,
    )


def build_sysu_annotations_from_file(
    dataset_folder: Path,
) -> pd.DataFrame:
    """Build a Dataframe for the SYSU annotations.
    The columns are:
        - split_sysu: train or query, gallery for test
        - is_hard difficult detection
        - bbox (x_min, y_min, width, height)

    Explanation of query and gallery split for test:

    For one person_id there is one detection as query and 100 detections
    as gallery.
    Among gallery's detections there are some with the query person. These row
    have values. For other, frame without the query person, is_hard and bbox
    columns have pd.NA values.

    Args:
        dataset_folder: path to the dataset folder

    Returns:
        pd.DataFrame: SYSU annotations dataframe.
    """
    (
        matlab_annotations_train,
        matlab_annotations_test,
        matlab_annotations_pool,
        matlab_annotations_images,
    ) = _open_sysu_annotations(dataset_folder)
    return _build_sysu_annotations(
        matlab_annotations_train,
        matlab_annotations_test,
        matlab_annotations_pool,
        matlab_annotations_images,
    )
