import json
import re
from pathlib import Path
from typing import Literal, TypedDict

import pandas as pd

from cuhk_sysu_pedes.constants import INDEXES

DTYPES = ["category", object, object]
COLUMN_NAMES = ["split_pedes", "caption_1", "caption_2"]
COLUMN_NAME_TO_DTYPE = {
    colum_name: dtype
    for colum_name, dtype in zip(COLUMN_NAMES, DTYPES)
}

JSON_FILENAME = "reid_raw.json"


class PEDESAnnotation(TypedDict):
    """
    This dict represents the structure of the json file
    for PEDES annotations
    """

    id: int
    file_path: str
    captions: tuple[str, str]
    split: Literal["train"] | Literal["test"] | Literal["val"]


def _import_pedes_annotations_from_json(
    json_file: Path,
) -> list[PEDESAnnotation]:
    with open(json_file, "r", encoding="utf-8") as f:
        annotations = json.load(f)

    # 'query' is in 'train' and 'test' file_path values in each annotation
    return [
        annotation
        for annotation in annotations
        if "query" in annotation["file_path"]
    ]


def build_pedes_annotations_from_file(
    dataset_folder: Path,
) -> pd.DataFrame:
    """Build a Dataframe for the PEDES annotations.

    The keys are:
        - person_id and frame_id
    The values are:
        - split ("train"/"test"/"val")
        - caption_1: first caption of PEDES
        - caption_2: second caption of PEDES

    Args:
        dataset_folder: path to the dataset folder

    Returns:
        pandas.DataFrame: PEDES annotations dataframe
    """
    json_annotations: list[
        PEDESAnnotation
    ] = _import_pedes_annotations_from_json(
        Path(dataset_folder) / JSON_FILENAME
    )

    numbers_expression = re.compile(r"[\d]+")
    crop_indexes = [
        tuple(
            map(
                int,
                numbers_expression.findall(
                    annotation["file_path"]
                ),
            )
        )
        for annotation in json_annotations
    ]

    splits_values = [
        annotation["split"] for annotation in json_annotations
    ]
    captions_1 = [
        str(annotation["captions"][0]).strip()
        for annotation in json_annotations
    ]
    captions_2 = [
        str(annotation["captions"][1]).strip()
        for annotation in json_annotations
    ]

    return pd.DataFrame(
        data={
            COLUMN_NAMES[0]: splits_values,
            COLUMN_NAMES[1]: captions_1,
            COLUMN_NAMES[2]: captions_2,
        },
        index=pd.MultiIndex.from_tuples(
            crop_indexes, names=INDEXES
        ),
    ).astype(COLUMN_NAME_TO_DTYPE)
