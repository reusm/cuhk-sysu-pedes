from typing import TypedDict

import pandas as pd


class _AnnotationsCOCOCategories(TypedDict):
    id: int
    name: str
    supercategory: str


class _AnnotationsCOCOImages(TypedDict):
    id: int
    file_name: str
    width: int
    height: int


class _AnnotationsCOCOAnnotations(TypedDict):
    # ID fields
    detection_id: int
    image_id: int
    category_id: int
    # Detection annotations
    bbox: tuple[int, int, int, int]
    area: int
    # Added annotations
    person_id: int  # Added on AlignPS
    captions: tuple[str, str]  # Added for fusing annotations
    # Useless legacy annotations
    iscrowd: int  # 0
    segmentation: list  # []


class AnnotationsCOCO(TypedDict):
    categories: list[_AnnotationsCOCOCategories]
    images: list[_AnnotationsCOCOImages]
    annotations: list[_AnnotationsCOCOAnnotations]


IMAGE_WIDTH, IMAGE_HEIGHT = 600, 800


def json_serialize_to_coco_format(
    annotations: pd.DataFrame,
) -> AnnotationsCOCO:
    """Serialize to match COCO format.
    This is a "big picture" function, it takes the annotations from split
    and turn them into COCO dictionnary `_AnnotationsCOCO`.

    Args:
        annotations (tuple[pd.DataFrame, pd.DataFrame]): _description_

    Returns:
        tuple[_AnnotationsCOCO, _AnnotationsCOCO]: train and test COCO format
        annotations.
    """
    # Init with categories keys/values done
    coco_annotations: AnnotationsCOCO = {
        "categories": [
            _AnnotationsCOCOCategories(
                id=1,
                name="person",
                supercategory="object",
            ),
        ],
        "images": list(),
        "annotations": list(),
    }

    # Detections only for COCO json
    annotations_with_detections = annotations.dropna(
        subset=["bbox_x", "bbox_y", "bbox_w", "bbox_h"]
    )

    frame_ids_unique = set(
        annotations_with_detections.index.get_level_values(
            "frame_id"
        )
    )
    coco_annotations["images"] = [
        _AnnotationsCOCOImages(
            id=frame_id,
            file_name=f"s{frame_id}.jpg",
            width=IMAGE_WIDTH,
            height=IMAGE_HEIGHT,
        )
        for frame_id in frame_ids_unique
    ]

    for i, (df_index, annotation) in enumerate(
        annotations_with_detections.iterrows()
    ):
        person_id, frame_id = df_index

        area = annotation.bbox_w * annotation.bbox_w
        bbox = (
            int(annotation.bbox_x),
            int(annotation.bbox_y),
            int(annotation.bbox_w),
            int(annotation.bbox_h),
        )

        captions = (
            ("", "")
            if person_id == -1
            else (
                str(annotation.caption_1),
                str(annotation.caption_2),
            )
        )

        coco_annotations["annotations"].append(
            _AnnotationsCOCOAnnotations(
                # COCO IDs field
                detection_id=i,
                image_id=frame_id,
                category_id=1,
                # Detection annotations
                bbox=bbox,
                area=area,
                # ReID extra annotations
                person_id=person_id,
                captions=captions,
                # Not used annotations
                iscrowd=0,
                segmentation=[],
            )
        )

    return coco_annotations
