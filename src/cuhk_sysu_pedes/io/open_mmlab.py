"""
The functions convert the dataframes to 2 JSON dictionaries following the
Open MMlab annotations format. One JSON by split.


See https://mmengine.readthedocs.io/en/latest/advanced_tutorials/basedataset.html#the-standard-data-annotation-file # noqa: E501

Content of .json files are in structure `OpenMMLabFormat`.

The tree of the dataset folder should be like this.
data
├── annotations
│   ├── train.json
│   └── test.json
├── train
│   ├── sysu
│   ├── s1.jpg
│   ├── s2.jpg
│   └── ...
└── test
    ├── s3.jpg
    ├── s8.jpg
    └── ...
"""
from typing import Literal, TypeAlias, TypedDict

import pandas as pd

SPLIT: TypeAlias = Literal["train"] | Literal["test"]

UNLABELED_PERSON_ID = Literal[-1]
UNLABELED_CAPTIONS = tuple[Literal[""], Literal[""]]
PERSON_DETECTION_LABEL = Literal[0]


class DetectionAnnotation(TypedDict):
    # NOTE: `detection_id` is the same key as `id` in COCO annotations.
    detection_id: int
    bbox: tuple[int, int, int, int]
    # class of the detection, only "person" class.
    label: PERSON_DETECTION_LABEL
    person_id: int | UNLABELED_PERSON_ID
    captions: tuple[str, str] | UNLABELED_CAPTIONS
    is_hard: bool


class OpenMMLabAnnotation(TypedDict):
    # Those two keys are mandatory.
    # To be more consistent with this base code we could name them
    # `frame_path` and `frame_id`
    img_path: str
    img_label: int
    detection_annotations: list[DetectionAnnotation]


class OpenMMlabMetainfo(TypedDict):
    # In detection person ReID it's the litteral ['person']
    categories: list[str]
    number_person_ids: int
    split: SPLIT


class OpenMMLabFormat(TypedDict):
    metainfo: OpenMMlabMetainfo
    data_list: list[OpenMMLabAnnotation]


class SerializedAnnotationsOpenMMLAB(TypedDict):
    train: OpenMMLabFormat
    test: OpenMMLabFormat


def _convert_df_to_open_mmlab_dicts(
    annotations: pd.DataFrame, split: SPLIT
) -> OpenMMLabFormat:
    number_person_ids = len(annotations.person_id.unique())
    sub_folder = f"{split}"

    data_list = []
    for idx_frame, (frame_id,
                    frame_annotations) in enumerate(annotations.groupby("frame_id")):
        img_path = f"{sub_folder}/s{frame_id}.jpg"
        detection_annotations: list[DetectionAnnotation] = []
        for idx_detection, detection_annotation in frame_annotations.iterrows():
            assert isinstance(idx_detection, int)
            captions = (
                (
                    str(detection_annotation.caption_1),
                    str(detection_annotation.caption_2),
                ) if pd.notna(detection_annotation.caption_1) else ("", "")
            )
            bbox = (
                int(detection_annotation.bbox_x),
                int(detection_annotation.bbox_y),
                int(detection_annotation.bbox_x) + int(detection_annotation.bbox_w),
                int(detection_annotation.bbox_y) + int(detection_annotation.bbox_h),
            )
            is_hard = detection_annotation.is_hard

            detection_annotations.append(
                DetectionAnnotation(
                    detection_id=idx_detection,
                    label=0,
                    person_id=detection_annotation.person_id,
                    captions=captions,
                    bbox=bbox,
                    is_hard=is_hard,
                )
            )
        data_list.append(
            OpenMMLabAnnotation(
                img_path=img_path,
                img_label=idx_frame,
                detection_annotations=detection_annotations,
            )
        )

    metainfo = OpenMMlabMetainfo(
        categories=["person"],
        number_person_ids=number_person_ids,
        split=split,
    )

    open_mmlab_annotations = OpenMMLabFormat(
        metainfo=metainfo,
        data_list=data_list,
    )
    return open_mmlab_annotations


# TODO Issue #19
def serialize_annotations_to_mmlab_format(
    annotations: pd.DataFrame,
) -> OpenMMLabFormat:

    train = annotations.query("split == 'train'").reset_index(level="person_id")
    serialized_annotations = _convert_df_to_open_mmlab_dicts(train, "train")

    return serialized_annotations
