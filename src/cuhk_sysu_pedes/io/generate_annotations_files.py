import json
from pathlib import Path
from typing import Mapping

import pandas as pd

import cuhk_sysu_pedes.pedes_annotations as pedes_annotations
import cuhk_sysu_pedes.sysu_annotations as sysu_annotations
from cuhk_sysu_pedes.constants import FORMATS, INDEXES
from cuhk_sysu_pedes.io.coco import (json_serialize_to_coco_format)
from cuhk_sysu_pedes.io.open_mmlab import (
    OpenMMLabFormat, serialize_annotations_to_mmlab_format
)

FILENAME = "annotations"

COLUMN_NAME_TO_DTYPE = (
    sysu_annotations.COLUMN_NAME_TO_DTYPE | pedes_annotations.COLUMN_NAME_TO_DTYPE
)


def _write_to_csv_file(
    annotations: pd.DataFrame,
    output_folder: Path,
) -> None:
    output_file = output_folder / f"{FILENAME}.csv"
    annotations.to_csv(output_file)


def write_to_json_file(
    json_serialized_annotations: Mapping[str, object],
    output_file: Path,
) -> None:
    with open(output_file, "w+", encoding="utf-8") as file:
        json.dump(
            json_serialized_annotations,
            file,
            indent=2,
        )


def _set_output_folder(output_folder_path: str) -> Path:
    if not (output_folder := Path(output_folder_path)).exists():
        Path.mkdir(output_folder)

    return output_folder


def _make_mmlab_annotations_files(
    annotations: pd.DataFrame, output_folder: Path
) -> None:
    mmlab_serialized_annotations = serialize_annotations_to_mmlab_format(annotations)

    for (
        base_dataset,
        serialized_annotations_by_base_dataset,
    ) in mmlab_serialized_annotations.items():  # type: ignore
        serialized_annotations_by_base_dataset: SerializedAnnotationsBasedataset
        for (
            split,
            serialized_annotations_by_split,
        ) in serialized_annotations_by_base_dataset.items():  # type: ignore
            serialized_annotations_by_split: OpenMMLabFormat
            filename = f"{FILENAME}_{base_dataset}_{split}.json"

            write_to_json_file(
                serialized_annotations_by_split,
                output_folder / filename,
            )


def write_annotations_to_file(
    annotations: pd.DataFrame,
    output_folder_path: str,
    formats: list[str],
) -> None:
    output_folder = _set_output_folder(output_folder_path)

    # CSV format (will be imported as dataframe)
    if FORMATS[0] in formats:
        _write_to_csv_file(
            annotations,
            output_folder,
        )

    # COCO format
    if FORMATS[1] in formats:
        write_to_json_file(
            json_serialize_to_coco_format(annotations),
            output_folder / f"{FILENAME}.json",
        )

    # Open MMLab format
    if FORMATS[2] in formats:
        _make_mmlab_annotations_files(annotations, output_folder)


def read_annotations_csv(annotations_path: str, ) -> pd.DataFrame:
    if not (annotation_file := Path(annotations_path)).exists():
        raise FileNotFoundError(f"File {annotation_file} does not exists.")

    return pd.read_csv(
        str(annotation_file),
        index_col=tuple(INDEXES),
        dtype=COLUMN_NAME_TO_DTYPE,
    )
