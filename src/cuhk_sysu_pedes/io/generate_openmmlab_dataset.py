"""
Generate the dataset folder with the following structure.
See module `open_mmlab.py` for information about Open MMLab format.

data
├── annotations
│   ├── train.json
│   └── test.json
├── train
│   ├── sysu
│   ├── s1.jpg
│   ├── s2.jpg
│   └── ...
└── test
    ├── s3.jpg
    ├── s8.jpg
    └── ...
"""
from pathlib import Path
from shutil import copyfile

import pandas as pd

from cuhk_sysu_pedes.io.generate_annotations_files import write_to_json_file
from cuhk_sysu_pedes.io.open_mmlab import (
    OpenMMLabFormat, serialize_annotations_to_mmlab_format
)

DATASET_FOLDER = "data"
SPLIT_FOLDERS = ["train", "test", "val"]
ANNOTATIONS_FOLDER = "annotations"
ANNOTATIONS_FILENAME = "annotations"
SYSU_FRAMES_FOLDER_PATH = "Image/SSM"


def _generate_annotations_files(
    serialized_annotations: OpenMMLabFormat,
    annotations_folder: Path,
    split: str,
):
    annotation_filename = f"{ANNOTATIONS_FILENAME}_{split}.json"
    write_to_json_file(serialized_annotations, annotations_folder / annotation_filename)


def _generate_frames_folder(
    serialized_annotations_by_split: OpenMMLabFormat,
    output_split_folder: Path,
    sysu_dataset_folder: Path,
) -> None:
    frame_filenames = [
        # img_path is set to have <split>/s<frame_id>.jpg
        data["img_path"] for data in serialized_annotations_by_split["data_list"]
    ]

    for frame_filename in frame_filenames:
        filename = frame_filename.split("/")[-1]
        source_file = sysu_dataset_folder / SYSU_FRAMES_FOLDER_PATH / filename
        destination_file = output_split_folder / filename
        copyfile(source_file, destination_file)


def generate_dataset(
    annotations: pd.DataFrame,
    output_folder_path: str,
    sysu_dataset_folder_path: str,
) -> None:
    # NOTE: generate_dataset is called after `build_annotations`, so sysu_dataset_folder_path
    # is a valid path.
    output_folder = Path(output_folder_path)
    sysu_dataset_folder = Path(sysu_dataset_folder_path)

    dataset_folder = output_folder / DATASET_FOLDER
    if dataset_folder.exists():
        print("No dataset generation, data folder already exists in output folder.")
        print("Please delete manually the data folder then generate again.")
        return

    serialized_annotations = serialize_annotations_to_mmlab_format(annotations)

    # Create data/annotations
    annotations_folder = dataset_folder / ANNOTATIONS_FOLDER
    annotations_folder.mkdir(parents=True)

    split = "train"

    # Create data/<split>
    output_split_folder = dataset_folder / split
    Path.mkdir(output_split_folder)

    # Create json in data/annotations
    _generate_annotations_files(
        serialized_annotations,
        annotations_folder,
        split,
    )
    # Fill folder data/<split>
    _generate_frames_folder(
        serialized_annotations,
        output_split_folder,
        sysu_dataset_folder,
    )
