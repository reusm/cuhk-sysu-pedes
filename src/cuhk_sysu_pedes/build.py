"""Functions used to build annotations.
You can choose between .csv for clarity or .json
for COCO style.
"""
from pathlib import Path

import pandas as pd

from cuhk_sysu_pedes.merge import merge_datasets
from cuhk_sysu_pedes.pedes_annotations import build_pedes_annotations_from_file
from cuhk_sysu_pedes.sysu_annotations import build_sysu_annotations_from_file


def build_annotations(
    sysu_folder_path: str,
    pedes_folder_path: str,
) -> pd.DataFrame:
    if not (sysu_folder := Path(sysu_folder_path)).exists():
        raise FileNotFoundError(f"Folder {sysu_folder_path} is not SYSU folder.")
    if not (pedes_folder := Path(pedes_folder_path)).exists():
        raise FileNotFoundError(f"Folder {pedes_folder_path} is not SYSU folder.")

    sysu_annotations = build_sysu_annotations_from_file(sysu_folder)
    pedes_annotations = build_pedes_annotations_from_file(pedes_folder)

    annotations = merge_datasets(pedes_annotations, sysu_annotations)

    return annotations
