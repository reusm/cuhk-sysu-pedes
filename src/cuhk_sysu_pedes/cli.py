"""Script to produce annotations files from CLI.

## Examples of usages:

- `python build_annotations_files.py ./data/CUHK-SYSU ./data/CUHK-PEDES` to
output csv file fused annotations from base directories.
- `python build_annotations_files.py ./data/CUHK-SYSU ./data/CUHK-PEDES -o json`
to output csv file fused annotations from base directories.
"""
import argparse

from cuhk_sysu_pedes.build import build_annotations
from cuhk_sysu_pedes.constants import FORMATS
from cuhk_sysu_pedes.io.generate_annotations_files import write_annotations_to_file
from cuhk_sysu_pedes.io.generate_openmmlab_dataset import generate_dataset

GENERATION_MODES = ["annotations", "dataset"]


def _set_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        prog="CUHK-SYSU-PEDES annotations builder",
        description="Fuse the annotations files from SYSU and PEDES datasets.",
        epilog="Credit: Mathias REUS.",
    )

    # Path of the datasets folders (not annotations)
    parser.add_argument("sysu_path", help="path to base CUHK-SYSU folder.")
    parser.add_argument("pedes_path", help="path to base CUHK-PEDES folder.")

    parser.add_argument(
        "-f",
        "--formats",
        nargs="+",
        # mmlab
        default=FORMATS[1],
        type=str,
        choices=FORMATS,
        metavar="format",
        help=f"Output format of the annotations files, csv for clarity and"
        f"coco for json COCO, mmlab for OpenMMLab usage.",
    )

    parser.add_argument(
        "-o",
        "--output-folder",
        type=str,
        default=".",
        help="Folder to output the annotations files.",
        dest="output_folder",
    )

    parser.add_argument(
        "--generation-modes",
        choices=GENERATION_MODES,
        nargs="+",
        default=GENERATION_MODES[1],
        help=f"Generate the annotations or the whole dataset. "
        f"Dataset generation will generate mmlab dataset.",
        dest="generation_modes",
    )
    return parser


def main() -> None:
    args = _set_parser().parse_args()

    annotations = build_annotations(args.sysu_path, args.pedes_path)

    # annotations files (csv) only
    if GENERATION_MODES[0] in args.generation_modes:
        write_annotations_to_file(
            annotations,
            args.output_folder,
            args.formats,
        )
        print(f"{' Annotations files created ':-^50s}")

    # mmlab dataset
    if GENERATION_MODES[1] in args.generation_modes:
        generate_dataset(
            annotations,
            args.output_folder,
            args.sysu_path,
        )
        print(f"{' Dataset generated ':-^50s}")


if __name__ == "__main__":
    main()
