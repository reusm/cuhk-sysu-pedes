"""
Merge PEDES and SYSU splits according to issue #17 and `corrupted_splits` notebook.
"""

from cuhk_sysu_pedes.constants import INDEXES
import pandas as pd

COMPLETE_SAMPLE_LENGTH = 101

TRAIN_SPLIT_NAME = 'train'
QUERY_SPLIT_NAME = 'query'
GALLERY_SPLIT_NAME = 'gallery'


def _is_complete_sample(sample: pd.DataFrame) -> bool:
    return len(sample) == COMPLETE_SAMPLE_LENGTH


def make_complete_gallery(
    gallery_annotations: pd.DataFrame, available_distractors: pd.DataFrame
) -> pd.DataFrame:
    n_distractor = 100 - len(gallery_annotations)

    distractors = available_distractors[:n_distractor]
    available_distractors = available_distractors[n_distractor:]

    full_gallery = pd.concat([gallery_annotations.droplevel("person_id"), distractors])

    return full_gallery


def _build_test_set(test_elements: pd.DataFrame) -> pd.DataFrame:
    """
    Build the test set from the test_elements, rows that are not in the train
    set.

    Args:
        test_elements (pd.DataFrame): Rows from annotations of PEDES and SYSU
        synced on CropIndex (person id, frame id) which are not in train set.

    Returns:
        pd.DataFrame: The correctly merged test set. It has the annotations
        of PEDES test and has samples (query/gallery, SYSU evaluation).
    """
    complete_samples = (
        test_elements \
        .groupby("person_id") \
        .filter(_is_complete_sample) \
    ).copy()
    complete_samples["split"] = complete_samples.split_sysu

    incomplete_samples = test_elements.loc[
        test_elements.index.difference(complete_samples.index) \
    ]
    incomplete_samples_annotations = incomplete_samples.dropna()


    queries = (
        incomplete_samples_annotations \
        .groupby("person_id") \
        .head(1) \
    ).copy()
    queries['split'] = QUERY_SPLIT_NAME

    gallery_annotations = incomplete_samples_annotations.loc[
        incomplete_samples_annotations.index.difference(queries.index) \
    ]

    available_distractors = incomplete_samples[incomplete_samples.isna()
                                               ].droplevel("person_id")
    completed_galleries = (
        gallery_annotations \
        .groupby("person_id") \
        .apply(make_complete_gallery, available_distractors) # type: ignore
    ).copy()
    completed_galleries["split"] = GALLERY_SPLIT_NAME

    completed_samples = pd.concat([queries, completed_galleries])

    return pd.concat([complete_samples, completed_samples])


def _get_train_indexes(annotations: pd.DataFrame) -> pd.Index:
    """
    Extract the train indexes from the merged annotations dataframe.

    These train indexes are from the PEDES train (ReID) annotations and SYSU
    train (detections) annotations.

    Args:
        annotations (pd.DataFrame): from SYSU and PEDES, synced on CropIndex
        (person id, frame id).

    Returns:
        pd.Index: indexes of train samples
    """
    train_reid_indexes = annotations.query("split_pedes in ('train', 'val')").index

    person_ids = annotations.index.get_level_values("person_id")
    train_detections_indexes = annotations[person_ids == -1].index.unique()

    return train_reid_indexes.union(train_detections_indexes)


def merge_datasets(
    pedes_annotations: pd.DataFrame,
    sysu_annotations: pd.DataFrame,
) -> pd.DataFrame:
    annotations = pd.merge(
        pedes_annotations,
        sysu_annotations,
        on=INDEXES,
        how="right",
    )

    train_indexes = _get_train_indexes(annotations)

    test_elements = annotations.loc[annotations.index.difference(train_indexes)]
    test = _build_test_set(test_elements)

    train = annotations.loc[train_indexes].copy()
    train["split"] = TRAIN_SPLIT_NAME

    merged_annotations = pd.concat([train, test]).astype({'split': 'category'})

    return merged_annotations
